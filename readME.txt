####CONVERT AND COMBINE MULTIPLE PDF FILES (UNLIMITED PAGES) TO CSV

#HOW TO USE THIS CODE:

#STEP 1: Go to this link: https://bitbucket.org/missusmilady/pdf-to-csv-in-r/downloads/

#STEP 2: Click 'Download Repository'. In the pop-up window, select 'Save File' and 'OK'

#STEP 3: Cut and Paste the downloaded repository (zipped folder) to your preferred location in your computer.
         #Extract the contents of the zipped repository
         #You may rename the downloaded repository (folder)

#STEP 4: Open the unzipped repository (folder)

#Step 5: Open the 'input' folder and put the pdf files you want to convert to .csv
         #filenames should start with the storecode, followed by "_" (e.g. '166_whateverfollows')
         #files should cover similar report periods

#STEP 6: Open 'main.R' for the next procedure. Edit the code as needed.